import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import stations from './stations';
import Select from 'react-select';
import { Table } from 'reactstrap';

class App extends Component {
  constructor(props) {
    super(props);
    let stationsArray = []
    stations.root.stations.station.forEach((item) => {
      stationsArray.push({ 'label': item.name, 'value': item.abbr });
    });
    this.state = { platformsLoading: true, platforms: [{}], loading: true, timetable: {}, stations: stationsArray, selectedOption: null, schedule: {} };
  }

  renderPlatforms() {
    if (this.state.platformsLoading === false) {
      return (
        <Table className="schedule" style={{ 'border': '2px solid white', 'borderRadius': '10px', 'marginTop': '15px' }}>
          <tbody>
            {this.state.platforms.map((item, i) =>
              <tr key={i}>
                <td className="timeUntil" style={{ 'color': item.hexcolor }}>
                  platform {item.platform} arrival time (m): {item.minutes} going ({item.direction})
                </td>
              </tr>
            )}
          </tbody>
        </Table>
      );
    } else {
      return null;
    }
  }

  renderSchedule() {
    if (this.state.loading === false) {
      return (
        <Table className="schedule" style={{ 'border': '2px solid white', 'borderRadius': '10px', 'marginTop': '15px' }}>
          <thead>
            <tr>
              <th>
                departing
            </th>
              <th>
                destination
            </th>
              <th>
                arrival time
            </th>
              <th>
                destination time
            </th>
            </tr>
          </thead>
          <tbody>
            {this.state.humanSchedule.map((item, i) =>
              <tr key={i}>
                <td>
                  {item.departing}
                </td>
                <td>
                  {item.destination}
                </td>
                <td>
                  {item.arrive}
                </td>
                <td>
                  {item.destTime}
                </td>
              </tr>
            )}
          </tbody>
        </Table>
      );
    } else {
      return <img src={logo} className="App-logo" alt="logo" />
    }
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption, loading: true, platformsLoading: true });
    fetch(`https://api.bart.gov/api/sched.aspx?cmd=stnsched&orig=${selectedOption.value}&key=MW9S-E7SL-26DU-VV8V&json=y`)
      .then((resp) => {
        return resp.json().then((d) => {

          let humanSchedule = []
          d.root.station.item.map((item, i) => {
            const humanDestination = stations.root.stations.station.filter((stationItem) => item["@trainHeadStation"] === stationItem.abbr)
            const humanObj = { 'departing': selectedOption.label, 'destination': humanDestination[0].name, "arrive": item["@origTime"], "destTime": item['@destTime'] }
            humanSchedule.push(humanObj);
            return humanSchedule;
          })
          this.setState({ humanSchedule: humanSchedule, loading: false });
        }
        ).then(() => fetch(`https://api.bart.gov/api/etd.aspx?cmd=etd&orig=${selectedOption.value}&key=MW9S-E7SL-26DU-VV8V&json=y`))
          .then((resp) => {
            resp.json().then((d) => {
              let platformArray = []
              d.root.station.forEach((item) => {
                item.etd.forEach((estimate) => {
                  let es = estimate.estimate[0];
                  platformArray.push(es);
                })
              }
              )
              this.setState({ 'platforms': platformArray, platformsLoading: false });
            }).catch(error => console.log(error));
          });
      })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Choose a station
          </p>
        </header>
        <Select
          placeholder='start typing. . .'
          value={this.state.selectedOption}
          onChange={this.handleChange}
          options={this.state.stations}
        />
        {this.renderPlatforms()}
        {this.renderSchedule()}

      </div>
    );
  }
}

export default App;
